import React from 'react';
import Board from './Board';
import Header from './Header';
import NewGame from './NewGame';
export default class Puzzle extends React.Component {
    constructor(props) {
      super(props);
        this.state = {
            originalboard: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0],
            board: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0],
            size: 4,
            time: 0,
            start: 0,
          isOn: false,
          totalMoves: 0
};
      this.newGame = this.newGame.bind(this);
      this.startTimer = this.startTimer.bind(this)
}
  
startTimer() {
    this.setState({
      time: this.state.time,
      start: Date.now() - this.state.time,
      isOn: true
    })
    this.timer = setInterval(() => this.setState({
      time: parseInt((Date.now() - this.state.start) / 1000)
    }), 1);
  }
  
newGame(size) {
      let board = new Array(size * size);
      for (let i = 0; i < size * size; ++i) board[i] = i;
      board = this.shuffle(board);
      this.updateBoard(board, size);
  this.setState({ size: size, time: 0, totalMoves: 0 });
  this.startTimer()
    }

updateBoard(board, size) {
(size === undefined) ? this.setState({ board: board, totalMoves: this.state.totalMoves + 1 }) : this.setState({ board: board })}

shuffle(o) {
      const temp = o.slice();
      for(let j, x, i = temp.length; i; j = Math.floor(Math.random() * i), x = temp[--i], temp[i] = temp[j], temp[j] = x);
      return temp;
    }
    componentDidMount() {
    this.newGame(4);
    }
    render() {
        return (
        <div className='container'>
          <div className='row'>
            <div className="col-lg-12 text-center">
        <Header />
            </div>
          </div>
          <div className='row'>
            <div className="col-lg-12 text-center">
                {this.state && this.state.board ? <Board size={this.state.size} board={this.state.board} originalBoard={this.state.originalboard} updateBoard={this.updateBoard.bind(this)}/> : null}
</div></div>
            <div className='row'>
            <div className="col-lg-12 text-center">
                <h3>Timer: {this.state.time} | Moves: {this.state.totalMoves}</h3>
            </div>
            </div>
            <div className='row'>
            <div className="col-lg-12 text-center">
                <NewGame caption={'Reload'} onClickHandler={this.newGame.bind(this, 4)} />
            </div>
          </div>
        </div>
      );
    }
};