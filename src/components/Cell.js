import React from 'react';
const Cell = (props) => {
    if (props.value === 0) {
        if (props.value === props.originalBoardIndexVal) {
            return (<span key={props.value} className='square zero matchedCell' onClick={() => props.clickHandler()}>&nbsp;</span>)
        }
        else {
            return (<span key={props.value} className='square zero' onClick={() => props.clickHandler()}>&nbsp;</span>)
        }
    }
    else {
    if (props.value === props.originalBoardIndexVal) {
            return(<span key={props.value} className='square matchedCell' onClick={() => props.clickHandler()}>{props.value}</span>)
        }
        else {
            return(<span key={props.value} className='square' onClick={() => props.clickHandler()}>{props.value}</span>)
        }
}
}
export default Cell;