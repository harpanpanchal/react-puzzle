import React from 'react';
const NewGame = (props) => {
    return <input type='submit' value={props.caption} onClick={props.onClickHandler} />
};
export default NewGame;