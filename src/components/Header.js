import React from 'react';
const Header = () => {
    return (
        <React.Fragment>
             <h1>Puzzle game</h1>
          <p>Click on the squares to move them (only those next to the blank square)</p>
        </React.Fragment>
    )
};

export default Header;