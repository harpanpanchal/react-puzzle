import React from 'react';
import ReactDOM from 'react-dom';
import Puzzle from './components/Puzzle';
import './styles/main.scss';

export default class App extends React.Component{
  render(){
    return (
      <Puzzle/>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('root'));