# Important Points

1. Install Application: Type 'npm install' command to install all the dependencies from the Package.json for this Application.
2. Run 'npm start' to start the application.
3. Click on the cell adjoining the blank cell.

Please note that if the cell has the number which should be the correct place for the number the cell would be highlighted with some background color.

Timer will start automatically when the page loads or by clicking on 'Reload' button.

## Pending points

1. Once all the numbers are placed in the correct cells, game should be over and the message should be displayed on the screen. It should also stop the timer and disable click event on all the cells. Only 'Reload' button should be active. Clicking the same should reload the game and timer will start automatically.

2. Styled Components can be created to assign the styles to individual components.

3. Redux has not been used but, if needed, it can be used as well.

4. Testing functionality (Jest / Enzyme) has not been implemented so far.

5. Graphic / Visual design if provided, css style can be altered and enhanced to match the design.

### Components

1. Board.js: This component creates the board / container.
2. Cell.js: This component will create individual cells with the values including '0' wherever applicable.
3. Header.js: This component will display the heading / parapgraph as per the requirements.
4. NewGame.js: This component renders submit button which will be used to reload the game.
5. Puzzle.js: This component has all the main functions and structure of the application. Bootstrap framework has been used.
